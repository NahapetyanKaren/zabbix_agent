[CmdletBinding()]
Param (
    [Parameter(Mandatory = $false, Position = 0)]
    [pscredential]$creds,

    [Parameter(Mandatory = $false, Position = 0)]
    [string[]]$Servers,

    [Parameter(Mandatory = $false, Position = 0)]
    [string[]]$Agenttype,

    [Parameter(Mandatory = $false, Position = 0)]
    [string[]]$Templates,

    [Parameter(Mandatory = $false, Position = 0)]
    [pscredential]$zbx_creds

)

Import-Module PSZabbix
$Agenttype = $env:Agenttype
$FileName = "c:\Program Files\Zabbix Agent"
$srv = $Servers.Split(",")
$zbxagent = "zabbix_agent-6.0.10.zip"
$zbxagent2 = "zabbix_agent2-6.0.10.zip"

foreach ($server in $srv) {

    echo "#######################################################################"
    echo "                    Starting configuration $server                     "
    echo "#######################################################################"

    if ($env:Agenttype -eq 'Zabbix_Agent') {
        Copy-Item ".\$zbxagent" $('\\' + $server + '.digi.loc\c$\')
    }
    elseif ($env:Agenttype -eq 'Zabbix_Agent2') {
        Copy-Item ".\$zbxagent2" $('\\' + $server + '.digi.loc\c$\')
    }

    echo '-----------------------------------------------------------------------'
    echo "                   Checking Zabbix archive copy                        "
    echo '-----------------------------------------------------------------------'

    write-host "$env:Agenttype has been copyed"
    echo ''

    Invoke-Command -ComputerName $server -Authentication Credssp -Credential $creds -ScriptBlock {

        echo '-----------------------------------------------------------------------'
        echo "                 Checking Zabbix agent status state                    "
        echo '-----------------------------------------------------------------------'

        if ($Using:Agenttype -eq 'Zabbix_Agent') {
            if ((get-process "zabbix_agentd" -ea SilentlyContinue) -eq $Null) {
                write-host "Not Running"
            }
            else { 
                write-host "Running"
                Stop-Process -processname "zabbix_agentd" -Force
            }
        }
        elseif ($Using:Agenttype -eq 'Zabbix_Agent2') {
            if ((get-process "zabbix_agent2" -ea SilentlyContinue) -eq $Null) { 
                write-host "Not Running" 
            }
            else { 
                write-host "Running"
                Stop-Process -processname "zabbix_agent2" -Force
            }
        }
        start-sleep 5
        echo ''
        echo '-----------------------------------------------------------------------'
        echo "                  Deleting old Zabbix Agent folder                     "
        echo '-----------------------------------------------------------------------'

        
    
        if (Test-Path $Using:FileName) {
            Remove-Item $Using:FileName -Recurse -Force
            write-host "$Using:FileName has been deleted"
        }
        else {
            write-host "$Using:FileName doesn't exist"
        }
        
        echo ''
        echo '-----------------------------------------------------------------------'
        echo "       Creating Zabbix Agent folder and unziping Zabbix archive        "
        echo '-----------------------------------------------------------------------'

        mkdir "C:\Program Files\Zabbix Agent"

        if ($Using:Agenttype -eq 'Zabbix_Agent') {
            Expand-Archive -LiteralPath "C:\$Using:zbxagent" -DestinationPath "c:\Program Files\Zabbix Agent" 
            Move-Item "c:\Program Files\Zabbix Agent\bin\zabbix_agentd.exe" "c:\Program Files\Zabbix Agent"
            Move-Item "c:\Program Files\Zabbix Agent\conf\zabbix_agentd.conf" "c:\Program Files\Zabbix Agent"
        }
        elseif ($Using:Agenttype -eq 'Zabbix_Agent2') {
            Expand-Archive -LiteralPath "C:\$Using:zbxagent2" -DestinationPath "c:\Program Files\Zabbix Agent" 
            Move-Item "c:\Program Files\Zabbix Agent\bin\zabbix_agent2.exe" "c:\Program Files\Zabbix Agent"
            Move-Item "c:\Program Files\Zabbix Agent\conf\zabbix_agent2.conf" "c:\Program Files\Zabbix Agent"
            Move-Item "c:\Program Files\Zabbix Agent\conf\zabbix_agent2.d" "c:\Program Files\Zabbix Agent"
        }

        echo ''
        echo '-----------------------------------------------------------------------'
        echo "         Zabbix_Agent.zip has been deleted from disk C:                 "
        echo '-----------------------------------------------------------------------'
        if ($Using:Agenttype -eq 'Zabbix_Agent') {
            Remove-Item "C:\$Using:zbxagent" -Force
            write-host "$Using:zbxagent has been deleted from disk C:"
        }
        elseif ($Using:Agenttype -eq 'Zabbix_Agent2') {
            Remove-Item "C:\$Using:zbxagent2" -Force
            write-host "$Using:zbxagent2 has been deleted from disk C:"
        }
        echo ''
        echo ''
    }
    

    echo '-----------------------------------------------------------------------'
    echo "                             Templating                                "
    echo '-----------------------------------------------------------------------'

    write-host "Your selected Template is: ($Templates)"

    foreach ($template in $Templates) {
        if  ($server.Contains("nl")) {
            $ServerIP = '10.101.20.26'
            $GroupID = '58'
            if ($template -eq "Windows OS Standard"){
                $tmplID = '10331'
            }
            elseif ($template -eq "OS Linux with agent") {
                $tmplID = '10736'
            }
            $TemplateID = $tmplID
            $ZBXHost = "https://nl-zbx.digi.loc/api_jsonrpc.php"      
        }
        elseif ($server.Contains("ix"))
        {
            $ServerIP = '10.104.20.35' 
            $GroupID = '50'
            if ($template -eq "Windows OS Standard"){
                $tmplID = '10849'
            }
            elseif ($template -eq "OS Linux with agent") {
                $tmplID = '10853'
            }
            $TemplateID = $tmplID
            $ZBXHost = "https://ix-zbx.digi.loc/api_jsonrpc.php"
        }
        else 
        {
            $ServerIP = '192.168.3.19'
            $GroupID = '59'
            if ($template -eq "Windows OS Standard"){
                $tmplID = '10608'
            }
            elseif ($template -eq "OS Linux with agent") {
                $tmplID = '10736'
            }
            $TemplateID = $tmplID
            $ZBXHost = "https://evn-zbx.digi.loc/api_jsonrpc.php"
        }
    }

    echo ''
    echo ''
    echo '-----------------------------------------------------------------------'
    echo "                Changing zabbix agent config File                      "
    echo '-----------------------------------------------------------------------'

    Invoke-Command -ComputerName $server -Authentication Credssp -Credential $creds -ScriptBlock {
        $newIP = "$($args[0])"
        if ($Using:Agenttype -eq "Zabbix_Agent") {
            (Get-Content -Path "c:\Program Files\Zabbix Agent\zabbix_agentd.conf") | ForEach-Object { $_ -Replace "127.0.0.1", $newIP } | Set-Content -Path "c:\Program Files\Zabbix Agent\zabbix_agentd.conf"
                write-host Changing IP from "127.0.0.1" to "$newIP"
            (Get-Content -Path "c:\Program Files\Zabbix Agent\zabbix_agentd.conf") | ForEach-Object { $_ -Replace 'Windows host', (Hostname).ToUpper() } | Set-Content -Path "c:\Program Files\Zabbix Agent\zabbix_agentd.conf"
                write-host Changing Hostname from "Windows host" to "$Using:server".ToUpper()
            (Get-Content -Path "c:\Program Files\Zabbix Agent\zabbix_agentd.conf") -replace "# Timeout=3", "Timeout=30" | Set-Content -Path "c:\Program Files\Zabbix Agent\zabbix_agentd.conf"
                write-host Enabled "Timeout" and changing from "3" to "30"
    
            c:\'Program Files'\'Zabbix Agent'\zabbix_agentd.exe --config c:\'Program Files'\'Zabbix Agent'\zabbix_agentd.conf --install
            Start-Service -Name 'Zabbix Agent'
            write-host "$Using:Agenttype installed successfully"
        }
        elseif ($Using:Agenttype -eq "Zabbix_Agent2") {
            (Get-Content -Path "c:\Program Files\Zabbix Agent\zabbix_agent2.conf") | ForEach-Object { $_ -Replace "127.0.0.1", $newIP } | Set-Content -Path "c:\Program Files\Zabbix Agent\zabbix_agent2.conf"
                write-host Changing IP from "127.0.0.1" to "$newIP"
            (Get-Content -Path "c:\Program Files\Zabbix Agent\zabbix_agent2.conf") | ForEach-Object { $_ -Replace 'Windows host', (Hostname).ToUpper() } | Set-Content -Path "c:\Program Files\Zabbix Agent\zabbix_agent2.conf"
                write-host Changing Hostname from "Windows host" to "$Using:server".ToUpper()
            (Get-Content -Path "c:\Program Files\Zabbix Agent\zabbix_agent2.conf") -replace "# Timeout=3", "Timeout=30" | Set-Content -Path "c:\Program Files\Zabbix Agent\zabbix_agent2.conf"
                write-host Enabled "Timeout" and changing from "3" to "30"
    
            c:\'Program Files'\'Zabbix Agent'\zabbix_agent2.exe --config c:\'Program Files'\'Zabbix Agent'\zabbix_agent2.conf --install
            Start-Service -Name 'Zabbix Agent 2'
            write-host "$Using:Agenttype installed successfully"
        }

        echo ''
        echo ''
        echo '-----------------------------------------------------------------------'
        echo "                Going to start zabbix agent service                    "
        echo '-----------------------------------------------------------------------'
        
        if ($Using:Agenttype -eq "Zabbix_Agent") {
            if ((get-process "zabbix_agentd" -ea SilentlyContinue) -eq $Null) {
                write-host "Not Running"
            }
            else { 
                write-host "Zabbix agent is Running"
            }
        }
        elseif ($Using:Agenttype -eq "Zabbix_Agent") {
            if ((get-process "zabbix_agent2" -ea SilentlyContinue) -eq $Null) {
                write-host "Not Running"
            }
            else { 
                write-host "Zabbix agent2 is Running"
            }
        }
    
    } -ArgumentList $ServerIP -ErrorAction SilentlyContinue

    echo ''
    echo ''
    echo '-----------------------------------------------------------------------'
    echo "                   Adding $server in Zabbix                            "
    echo '-----------------------------------------------------------------------'
    
    try {
        $session = New-ZbxApiSession $ZBXHost -auth $zbx_creds
        $ips = [System.Net.Dns]::GetHostAddresses("$server") | Select-Object -ExpandProperty  IPAddressToString
        if ($session -ne $null) {
            Write-Host "`nSession to $ZBXHost established"
            $newHost = New-ZbxHost -Name $server.ToUpper() -HostGroupId $GroupID -TemplateId $TemplateID -Dns $ips -Port 10050
            Write-Host "The "$server.ToUpper()" created successfully"
        }
    }
    catch {
        $Error[0]
        $_
    }
    
    echo ''
    echo ''
}